export default {
  "touchiconMimeType": "image/png",
  "faviconMimeType": "image/png",
  "precomposed": false,
  "touchicons": [
    {
      "width": 76,
      "src": "/assets/static/src/favicon.png?width=76&key=11f2094"
    },
    {
      "width": 152,
      "src": "/assets/static/src/favicon.png?width=152&key=11f2094"
    },
    {
      "width": 120,
      "src": "/assets/static/src/favicon.png?width=120&key=11f2094"
    },
    {
      "width": 167,
      "src": "/assets/static/src/favicon.png?width=167&key=11f2094"
    },
    {
      "width": 180,
      "src": "/assets/static/src/favicon.png?width=180&key=11f2094"
    }
  ],
  "favicons": [
    {
      "width": 16,
      "src": "/assets/static/src/favicon.png?width=16&key=b128847"
    },
    {
      "width": 32,
      "src": "/assets/static/src/favicon.png?width=32&key=b128847"
    },
    {
      "width": 96,
      "src": "/assets/static/src/favicon.png?width=96&key=b128847"
    }
  ]
}